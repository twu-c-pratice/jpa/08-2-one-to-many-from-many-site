package com.twuc.webApp.domain.oneToMany.manyToOneOnly;

import com.twuc.webApp.domain.ClosureValue;
import com.twuc.webApp.domain.JpaTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;

class ChildEntityRepositoryTest extends JpaTestBase {

    @Autowired private ChildEntityRepository childEntityRepository;

    @Autowired private ParentEntityRepository parentEntityRepository;

    @Test
    void should_save_parent_and_child_entity() {
        // TODO
        //
        // 请书写测试存储一对 one-to-many 的 child-parent 关系。并从 child 方面进行查询来证实存储已经
        // 成功。
        //
        // <--start-
        ClosureValue<Long> childId = new ClosureValue<>();

        flushAndClear(
                em -> {
                    ParentEntity parent = new ParentEntity("Ocean");
                    ChildEntity child = new ChildEntity("Sea");
                    child.setParent(parent);

                    parentEntityRepository.save(parent);
                    childEntityRepository.save(child);
                    childId.setValue(child.getId());
                });

        run(
                em -> {
                    ChildEntity savedChild =
                            childEntityRepository.findById(childId.getValue()).get();

                    assertEquals("Sea", savedChild.getName());
                });

        // --end-->
    }

    @Test
    void should_remove_the_child_parent_relationship() {
        // TODO
        //
        // 请书写测试：
        //
        // Given 一对 one-to-many 的 child-parent 关系。
        // When 解除 child 和 parent 的关系
        // Then child 和 parent 仍然存在，但是 child 不再引用 parent。
        //
        // <--start-
        ClosureValue<Long> childId = new ClosureValue<>();
        ClosureValue<Long> parentId = new ClosureValue<>();

        flushAndClear(
                em -> {
                    ParentEntity parent = new ParentEntity("Ocean");
                    ChildEntity child = new ChildEntity("Sea");
                    child.setParent(parent);

                    parentEntityRepository.save(parent);
                    childEntityRepository.save(child);

                    childId.setValue(child.getId());
                    parentId.setValue(child.getId());
                });

        flushAndClear(
                em -> {
                    ChildEntity child = childEntityRepository.findById(childId.getValue()).get();
                    child.setParent(null);
                });

        run(
                em -> {
                    assertEquals(
                            "Ocean",
                            parentEntityRepository.findById(parentId.getValue()).get().getName());
                    assertEquals(
                            "Sea",
                            childEntityRepository.findById(childId.getValue()).get().getName());
                    assertNull(
                            childEntityRepository.findById(childId.getValue()).get().getParent());
                });

        // --end-->
    }

    @Test
    void should_remove_child_and_parent() {
        // TODO
        //
        // 请书写测试：
        //
        // Given 一对 one-to-many 的 child-parent 关系。
        // When 删除 child 和 parent。
        // Then child 和 parent 不再存在。
        //
        // <--start-
        ClosureValue<Long> childId = new ClosureValue<>();
        ClosureValue<Long> parentId = new ClosureValue<>();

        flushAndClear(
                em -> {
                    ParentEntity parent = new ParentEntity("Ocean");
                    ChildEntity child = new ChildEntity("Sea");
                    child.setParent(parent);

                    parentEntityRepository.save(parent);
                    childEntityRepository.save(child);

                    childId.setValue(child.getId());
                    parentId.setValue(child.getId());
                });

        flushAndClear(
                em -> {
                    ParentEntity savedParent =
                            parentEntityRepository.findById(parentId.getValue()).get();
                    ChildEntity savedChild =
                            childEntityRepository.findById(childId.getValue()).get();

                    parentEntityRepository.delete(savedParent);
                    childEntityRepository.delete(savedChild);
                });

        run(
                em -> {
                    assertFalse(parentEntityRepository.findById(parentId.getValue()).isPresent());
                    assertFalse(childEntityRepository.findById(childId.getValue()).isPresent());
                });
        // --end-->
    }
}
