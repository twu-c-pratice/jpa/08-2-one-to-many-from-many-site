package com.twuc.webApp.domain.oneToMany.manyToOneOnly;

import javax.persistence.*;

// TODO
//
// 请定义 ParentEntity。其中 ParentEntity 不引用 ChildEntity。并且其对应的数据表定义应当满足如下
// 的条件：
//
// parent_entity
// +─────────+──────────────+──────────────────────────────+
// | Column  | Type         | Additional                   |
// +─────────+──────────────+──────────────────────────────+
// | id      | bigint       | primary key, auto_increment  |
// | name    | varchar(20)  | not null                     |
// +─────────+──────────────+──────────────────────────────+
//
// <--start-
@Entity
public class ParentEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String name;

    //    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "parent")
    //    private List<ChildEntity> children = new ArrayList<>();

    public ParentEntity() {}

    public ParentEntity(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
// --end-->
